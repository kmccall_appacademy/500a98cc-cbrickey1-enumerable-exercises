require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |phrase| phrase.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  array_of_letters = []
  no_spaces = string.gsub(/\s+/, "")

  no_spaces.each_char.with_index do |letter, index_1|
    (index_1 + 1).upto(no_spaces.length - 1) do |index_2|
      if (letter == no_spaces[index_2]) && (array_of_letters.include?(letter) == false)
        array_of_letters << letter
      end
    end
  end

  array_of_letters.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  longest_two_array = []

  word_array = string.split(/\s+/)
  sorted_word_array = word_array.sort_by { |word| word.length }

  longest_two_array << sorted_word_array.pop
  longest_two_array << sorted_word_array.pop
  longest_two_array
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
    'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

  alphabet.reject { |letter| string.include?(letter) }

end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  unique_array = []

  year.to_s.split("").each do |digit|
    if unique_array.include?(digit)
      return false
    else
      unique_array << digit
    end
  end

  true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.uniq.select { |song| no_repeats?(song, songs) }
end


def no_repeats?(song_name, songs)
  repeats = []

  0.upto(songs.length - 2) do |idx|
    if songs[idx] == songs[idx + 1]
      repeats << songs[idx]
    end
  end

  unique_repeats = repeats.uniq
  return false if unique_repeats.include?(song_name)
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  word_array = string.split(/\s+/)
  word_array.map! { |word| word.gsub(/[!@#$%^&*()-=_+|;':",.<>?']/, '') }

  word_data_array = word_array.map.with_index do |word, index|
    if c_distance(word) == "no c"
      [999, index, word]
    else
      [c_distance(word), index, word]
    end
  end

  word_data_array.sort[0][2]
end


def c_distance(word)
  -1.downto(-1 * word.length) do |idx|
    return (-1 * idx) if word[idx].downcase == "c"
  end

  "no c"
end


# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  array_of_indices = []
  data_array = arr.map.with_index { |number, index| [number, index] }

  while data_array.length > 1

    0.upto(data_array.length - 2) do |index1|

      current_item = data_array[index1][0]
      next_item = data_array[index1 + 1][0]

      if (data_array.length - 1) == (index1 + 1) && (current_item != next_item)
        return array_of_indices           #exit all loops because reached the end
      elsif current_item != next_item
        next                              #go to next iteration of index1
      else
        start_index = index1              #proceed to INNER LOOP which looks for subsequent items that match current_item (beyond index1 + 1)
        stop_index = index1 + 1


        #INNER LOOP
        (index1 + 1).upto(data_array.length - 1) do |index2|
          break unless data_array[start_index][0] == data_array[index2][0]
          stop_index = index2
        end


        array_of_indices << [data_array[start_index][1], data_array[stop_index][1]]
        data_array = data_array[(stop_index + 1)..-1]
      end#of if/else

      break
    end#of outer loop

  end#of while statement

  array_of_indices
end
